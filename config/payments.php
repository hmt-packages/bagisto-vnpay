<?php

return [
    'vnpay' => [
        'code'             => 'vnpay',
        'title'            => 'VNPAY',
        'description'      => 'VNPAY',
        'version'          => '2.1.0',
        'secure_hash_type' => 'SHA256',
        'class'            => 'HMT\VNPAY\Payments\VNPAY',
        'active'           => true,
        'sort'             => 4,
    ],
];
