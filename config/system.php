<?php

return [
    [
        'key'    => 'sales.paymentmethods.vnpay',
        'name'   => 'vnpay::app.admin.system.vnpay',
        'sort'   => 4,
        'fields' => [
            [
                'name'          => 'logo',
                'title'         => 'admin::app.admin.system.logo-image',
                'type'          => 'image',
            ],
            [
                'name'          => 'title',
                'title'         => 'admin::app.admin.system.title',
                'type'          => 'depends',
                'depend'        => 'active:1',
                'validation'    => 'required_if:active,1',
                'channel_based' => false,
                'locale_based'  => true,
            ],
            [
                'name'          => 'description',
                'title'         => 'admin::app.admin.system.description',
                'type'          => 'textarea',
                'channel_based' => false,
                'locale_based'  => true,
            ],
            [
                'name'          => 'version',
                'title'         => 'vnpay::app.admin.system.version',
                'info'          => 'vnpay::app.admin.system.version-info',
                'type'          => 'depends',
                'depend'        => 'active:1',
                'validation'    => 'required_if:active,1',
            ],
            [
                'name'          => 'tmn_code',
                'title'         => 'vnpay::app.admin.system.tmn_code',
                'info'          => 'vnpay::app.admin.system.tmn_code-info',
                'type'          => 'depends',
                'depend'        => 'active:1',
                'validation'    => 'required_if:active,1',
            ],
            [
                'name'          => 'hash_secret',
                'title'         => 'vnpay::app.admin.system.hash_secret',
                'info'          => 'vnpay::app.admin.system.hash_secret-info',
                'type'          => 'depends',
                'depend'        => 'active:1',
                'validation'    => 'required_if:active,1',
            ],
            [
                'name'          => 'secure_hash_type',
                'title'         => 'vnpay::app.admin.system.secure_hash_type',
                'info'          => 'vnpay::app.admin.system.secure_hash_type-info',
                'type'          => 'select',
                'options'       => [
                    [
                        'title' => 'SHA256',
                        'value' => 'SHA256',
                    ], [
                        'title' => 'HmacSHA512',
                        'value' => 'HmacSHA512',
                    ],
                ],
            ],
            [
                'name'          => 'live',
                'title'         => 'Live',
                'type'          => 'boolean',
                'validation'    => 'required',
                'channel_based' => false,
                'locale_based'  => false
            ],
            [
                'name'          => 'active',
                'title'         => 'admin::app.admin.system.status',
                'type'          => 'boolean',
                'validation'    => 'required',
                'channel_based' => false,
                'locale_based'  => true
            ],
            [
                'name'          => 'sort',
                'title'         => 'admin::app.admin.system.sort_order',
                'type'          => 'select',
                'options'       => [
                    [
                        'title' => '1',
                        'value' => 1,
                    ], [
                        'title' => '2',
                        'value' => 2,
                    ], [
                        'title' => '3',
                        'value' => 3,
                    ], [
                        'title' => '4',
                        'value' => 4,
                    ], [
                        'title' => '5',
                        'value' => 5,
                    ],
                ],
            ]
        ]
    ]
];
