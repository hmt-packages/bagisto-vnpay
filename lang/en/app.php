<?php

return [
    'admin' => [
        'system' => [
            'vnpay'                 => 'VNPAY',
            'version'               => 'vnp_Version',
            'version-info'          => 'Phiên bản api mà merchant kết nối. Phiên bản hiện tại là : 2.0.1 và 2.1.0',
            'tmn_code'              => 'vnp_TmnCode',
            'tmn_code-info'         => 'Mã website của merchant trên hệ thống của VNPAY. Ví dụ: 2QXUI4J4',
            'hash_secret'           => 'vnp_HashSecret',
            'hash_secret-info'      => 'Chuỗi bí mật sử dụng để kiểm tra toàn vẹn dữ liệu khi hai hệ thống trao đổi thông tin (checksum)',
            'secure_hash_type'      => 'vnp_SecureHashType',
            'secure_hash_type-info' => 'Loại mã băm sử dụng: SHA256, HmacSHA512',
        ],
    ],
];
