<?php

use Illuminate\Support\Facades\Route;

Route::middleware('web')
    ->controller(HMT\VNPAY\Http\Controllers\Shop\VNPAYController::class)
    ->prefix('checkout/vnpay')
    ->name('shop.checkout.vnpay.')
    ->group(function () {

        Route::get('/return', 'VNPAYController@return')
            ->name('return');

        Route::get('/ipn', 'VNPAYController@ipn')
            ->name('ipn');
    });
