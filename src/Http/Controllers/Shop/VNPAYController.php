<?php

namespace HMT\VNPAY\Http\Controllers\Shop;

use HMT\VNPAY\Payments\VNPAY;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Request;
use Webkul\Checkout\Facades\Cart;
use Webkul\Sales\Repositories\OrderRepository;

class VNPAYController extends Controller
{
    /**
     * @var OrderRepository $orderRepository
     */
    protected OrderRepository $orderRepository;

    /**
     * Create a new controller instance.
     *
     * @param  OrderRepository  $orderRepository
     */
    public function __construct(OrderRepository $orderRepository)
    {
        $this->orderRepository = $orderRepository;
    }

    /**
     * VNPAY return
     *
     * @param  Request  $request
     * @param  VNPAY    $vnpay
     * @return \Illuminate\Http\RedirectResponse
     */
    public function return(Request $request, VNPAY $vnpay)
    {
        $secureHash = $vnpay->getSecureHash($request->except(['vnp_SecureHashType', 'vnp_SecureHash']));

        if ($secureHash == $request->get('vnp_SecureHash') && $request->get('vnp_ResponseCode') === '00') {
            if (! ($cart = Cart::getCart()) || ! $cart->is_active) {
                session()->flash('warning', 'The cart is empty.');

                return redirect()->route('shop.checkout.cart.index');
            }

            if ($this->orderRepository->whereCartId($cart->id)->exists()) {
                $order = $this->orderRepository->whereCartId($cart->id)->orderByDesc('id')->first();
            } else {
                $order = $this->orderRepository->create(Cart::prepareDataForOrder());
                $order->payment_status = 1;
                $order->save();
            }

            Cart::deActivateCart();

            session()->flash('order', $order);

            return redirect()->route('shop.checkout.success');
        }

        session()->flash('warning', 'Payment failed!');

        return redirect()->route('shop.checkout.onepage.index');
    }

    /**
     * VNPAY IPN listener
     *
     * @param  Request  $request
     * @param  VNPAY    $vnpay
     * @return \Illuminate\Http\JsonResponse
     */
    public function ipn(Request $request, VNPAY $vnpay)
    {
        $inputData = array();
        $returnData = array();

        foreach ($request->all() as $key => $value) {
            if (str_starts_with($key, 'vnp_')) {
                $inputData[$key] = $value;
            }
        }

        $vnp_SecureHash = $inputData['vnp_SecureHash'];
        unset($inputData['vnp_SecureHash']);
        ksort($inputData);
        $hashData = urldecode(http_build_query($inputData));

        $secureHash = hash_hmac('sha512', $hashData, $vnp_HashSecret);
        $vnpTranId = $inputData['vnp_TransactionNo']; //Mã giao dịch tại VNPAY
        $vnp_BankCode = $inputData['vnp_BankCode']; //Ngân hàng thanh toán
        $vnp_Amount = $inputData['vnp_Amount']/100; // Số tiền thanh toán VNPAY phản hồi

        $Status = 0; // Là trạng thái thanh toán của giao dịch chưa có IPN lưu tại hệ thống của merchant chiều khởi tạo URL thanh toán.
        $cartId = $inputData['vnp_TxnRef'];

        try {
            //Check Orderid
            //Kiểm tra checksum của dữ liệu
            if ($secureHash == $vnp_SecureHash) {
                //Lấy thông tin đơn hàng lưu trong Database và kiểm tra trạng thái của đơn hàng, mã đơn hàng là: $orderId
                //Việc kiểm tra trạng thái của đơn hàng giúp hệ thống không xử lý trùng lặp, xử lý nhiều lần một giao dịch
                //Giả sử: $order = mysqli_fetch_assoc($result);
                //$order = $this->orderRepository->where('cart_id', $cartId)->first();

                $order = $this->orderRepository->whereCartId($cartId)
                                                ->orderByDesc('id')
                                                ->first();

                if ($order) {

                    if ($vnp_Amount != $order->grand_total) {
                        $returnData['RspCode'] = '04';
                        $returnData['Message'] = 'Invalid amount';
                    }
                    elseif ($inputData['vnp_ResponseCode'] != '00') {

                        // update fail payment
                        $order->payment_status = 2;
                        $order->save();

                        $returnData['RspCode'] = '00';
                        $returnData['Message'] = 'Confirm Success';
                    }
                    else {

                        if ($order->payment_status == 0) {

                            // update payment success
                            $order->payment_status = 1;
                            $order->save();

                            $returnData['RspCode'] = '00';
                            $returnData['Message'] = 'Confirm Success';
                        }
                        else {
                            $returnData['RspCode'] = '02';
                            $returnData['Message'] = 'Order already confirmed';
                        }
                    }
                } else {
                    $returnData['RspCode'] = '01';
                    $returnData['Message'] = 'Order not found';
                }
            } else {
                $returnData['RspCode'] = '97';
                $returnData['Message'] = 'Chu ky khong hop le';
            }
        } catch (\Exception $e) {
            $returnData['RspCode'] = '99';
            $returnData['Message'] = 'Unknow error';
        }

        //Trả lại VNPAY theo định dạng JSON
        return response()->json($returnData);
    }
}
