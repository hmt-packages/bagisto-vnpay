<?php

namespace HMT\VNPAY\Payments;

use Webkul\Payment\Payment\Payment;

class VNPAY extends Payment
{
    /**
     * Payment method code
     *
     * @var string
     */
    protected string $code = 'vnpay';

    /**
     * @return string
     */
    public function getRedirectUrl(): string
    {
        $this->createOrder();

        if (! empty($this->getConfigData('live'))) {
            return 'https://pay.vnpay.vn/vpcpay.html?' . http_build_query($this->getParams());
        } else {
            return 'https://sandbox.vnpayment.vn/paymentv2/vpcpay.html?' . http_build_query($this->getParams());
        }
    }

    /**
     * @return array
     */
    private function getParams(): array
    {
        $cart = cart()->getCart();

        $data = [
            'vnp_Version'    => $this->getConfigData('version'),
            'vnp_TmnCode'    => $this->getConfigData('tmn_code'),
            'vnp_Amount'     => $cart->grand_total * 100,
            'vnp_Command'    => 'pay',
            'vnp_CreateDate' => date('YmdHis'),
            'vnp_CurrCode'   => 'VND',
            'vnp_IpAddr'     => request()->ip(),
            'vnp_Locale'     => 'vn',
            'vnp_OrderInfo'  => "Thanh toan GD: {$cart->id}",
            'vnp_OrderType'  => 'other',
            'vnp_ReturnUrl'  => route('shop.checkout.vnpay.return'),
            'vnp_TxnRef'     => $cart->id,
        ];

        ksort($data);

        $data['vnp_SecureHash'] = $this->getSecureHash($data);

        return $data;
    }

    /**
     * @param  array  $data
     * @return false|string
     */
    public function getSecureHash(array $data): bool|string
    {
        $query = urldecode(http_build_query($data));

        return hash_hmac($this->getConfigData('secure_hash_type'), $query, $this->getConfigData('hash_secret'));
    }
}
